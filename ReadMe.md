# NC Sales Tax API
- Functions for retrieving tax rates in North Carolina. Public and unrestricted.

## TaxRatesController
### GetTaxByCounty

- Input:
    County (string)
- Output:
```json
    {
        "countyTaxRate":"2.50%",        "stateTaxRate":"4.75%", "totalTaxRate":"7.25%"
    }
```
- Sample call:
    - https://localhost:44333/api/TaxRates/GetTaxByCounty/Wake

# Thoughts
- The static class holding the tax info is not what a real world scenario would look like. See comment in class.
- The default controller was set up with Swagger for documentation. Definitely something I'd normally utilize but I'm trying to limit scope
- Next steps would possibly be Integration Tests or authorization via an Identity provider and/or a client of some kind. 
- There's plenty more to include in the .gitIgnore but it's a small enough total project I'm not overly concerned with it at the moment.