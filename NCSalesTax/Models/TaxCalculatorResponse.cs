﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NCSalesTax.Models
{
    /// <summary>
    /// Model representation of the JSON that will be returned by the GetTaxByCounty API Method
    /// </summary>
    public class TaxCalculatorResponse
    {
        public string CountyTaxRate { get; set; }
        public string StateTaxRate { get; set; }
        public string TotalTaxRate { get; set; }
    }
}
