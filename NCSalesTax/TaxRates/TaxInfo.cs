﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NCSalesTax
{
    /// <summary>
    /// Stores the Tax information for North Carolina in a Dictionary.
    /// </summary>
    public static class TaxInfo
    {
        public static decimal FindByCounty(string county)
        {
            string lower = county.ToLowerInvariant();
            if (CountyTaxRates.ContainsKey(lower))
            {
                return CountyTaxRates[lower];
            }
            else
            {
                return 0.0m;
            }
        }

        /*There are better solutions to storing this information, but the website allowing me to download the database gave me an error message saying, "Our online 
         * self service platform is currently down for maintenance and will be back soon." This is the dirty temporary solution.*/
        private static Dictionary<string, decimal> CountyTaxRates = new Dictionary<string, decimal>()
        {
            {"alamance", 6.75m},
            {"alexander", 7m},
            {"alleghany", 6.75m},
            {"anson", 7m},
            {"ashe", 7m},
            {"avery", 6.75m},
            {"beaufort", 6.75m},
            {"bertie", 7m},
            {"bladen", 6.75m},
            {"brunswick", 6.75m},
            {"buncombe", 7m},
            {"burke", 6.75m},
            {"cabarrus", 7m},
            {"caldwell", 6.75m},
            {"camden", 6.75m},
            {"carteret", 7m},
            {"caswell", 6.75m},
            {"catawba", 7m},
            {"chatham", 7m},
            {"cherokee", 7m},
            {"chowan", 6.75m},
            {"clay", 7m},
            {"cleveland", 6.75m},
            {"columbus", 6.75m},
            {"craven", 6.75m},
            {"cumberland", 7m},
            {"currituck", 6.75m},
            {"dare", 6.75m},
            {"davidson", 7m},
            {"davie", 6.75m},
            {"duplin", 7m},
            {"durham", 7.5m},
            {"edgecombe", 7m},
            {"forsyth", 7m},
            {"franklin", 6.75m},
            {"gaston", 7m},
            {"gates", 6.75m},
            {"graham", 7m},
            {"granville", 6.75m},
            {"greene", 7m},
            {"guilford", 6.75m},
            {"halifax", 7m},
            {"harnett", 7m},
            {"haywood", 7m},
            {"henderson", 6.75m},
            {"hertford", 7m},
            {"hoke", 6.75m},
            {"hyde", 6.75m},
            {"iredell", 6.75m},
            {"jackson", 7m},
            {"johnston", 6.75m},
            {"jones", 7m},
            {"lee", 7m},
            {"lenoir", 7m},
            {"lincoln", 7m},
            {"macon", 6.75m},
            {"madison", 7m},
            {"martin", 7m},
            {"mcdowell", 6.75m},
            {"mecklenburg", 7.25m},
            {"mitchell", 6.75m},
            {"montgomery", 7m},
            {"moore", 7m},
            {"nash", 7m},
            {"new Hanover", 7m},
            {"northampton", 6.75m},
            {"onslow", 7m},
            {"orange", 7.5m},
            {"pamlico", 6.75m},
            {"pasquotank", 7m},
            {"pender", 6.75m},
            {"perquimans", 6.75m},
            {"person", 7.5m},
            {"pitt", 7m},
            {"polk", 6.75m},
            {"randolph", 7m},
            {"richmond", 6.75m},
            {"robeson", 7m},
            {"rockingham", 7m},
            {"rowan", 7m},
            {"rutherford", 7m},
            {"sampson", 7m},
            {"scotland", 6.75m},
            {"stanly", 7m},
            {"stokes", 6.75m},
            {"surry", 7m},
            {"swain", 7m},
            {"Transylvania", 6.75m},
            {"tyrrell", 6.75m},
            {"union", 6.75m},
            {"vance", 6.75m},
            {"wake", 7.25m},
            {"warren", 7m},
            {"washington", 6.75m},
            {"watauga", 6.75m},
            {"wayne", 6.75m},
            {"wilkes", 7m},
            {"wilson", 7m},
            {"yadkin", 6.75m},
            {"yancey", 6.75m}
        };
    }
}
