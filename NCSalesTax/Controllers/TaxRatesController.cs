﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;

namespace NCSalesTax.Controllers
{
    /// <summary>
    /// Defines a single endpoint that allows the user to get the tax rate for their county in North Carolina
    /// </summary>
    [ApiController]
    [Route("api/TaxRates")]
    public class TaxRatesController : ControllerBase
    {
        /// <summary>
        /// supplies the user with the tax rates for their chosen county in North Carolina.
        /// </summary>
        /// <param name="county">The specified county</param>
        /// <returns>A Json representation of the state, county and total tax rates</returns>
        [HttpGet("GetTaxByCounty/{county}")]
        public ActionResult<Models.TaxCalculatorResponse> GetTaxByCounty(string county)
        {
            //Did not receive the county
            if (string.IsNullOrEmpty(county))
            {
                Trace.TraceError("Bad Request: Received no county information.");
                return BadRequest();
            }

            decimal tax = TaxInfo.FindByCounty(county);

            //Did not find the county the user is looking for
            if (tax == 0.0m)
            {
                Trace.TraceError("Not Found: Could not locate the specified county.");
                return NotFound();
            }

            //Return all relevant rates in a model response
            return Ok(new Models.TaxCalculatorResponse()
            {
                CountyTaxRate = String.Format("{0}%", (tax - 4.75m).ToString()),
                StateTaxRate = "4.75%",
                TotalTaxRate = String.Format("{0}%", tax.ToString())
            });
        }
    }
}
