﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NCSalesTax;

namespace NCSalesTaxUnitTests.TaxInfo_Unit_Tests
{
    /// <summary>
    /// Tests for the TaxInfo helper class that returns tax rates
    /// </summary>
    [TestClass]
    public class TaxInfoTests
    {
        [DataTestMethod]
        [DataRow("franklin", DisplayName = "FindByCounty_AllLowercase_Success")]
        [DataRow("FrAnKlIn", DisplayName = "FindByCountry_UpperAndLowercase_Success")]
        [DataRow("FRANKLIN", DisplayName = "FindByCountry_AllUppercase_Success")]
        public void TaxInfo_FindByCounty_Success(string county)
        {
            //Act
            decimal result = TaxInfo.FindByCounty(county);

            //Assert
            Assert.AreEqual(6.75m, result);
        }

        [TestMethod]
        public void TaxInfo_FindByCounty_NotFound_ZeroResult()
        {
            //Act
            decimal result = TaxInfo.FindByCounty("NOT a real county");

            //Assert
            Assert.AreEqual(0.0m, result);
        }
    }
}
