using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controllers = NCSalesTax.Controllers;
using Models = NCSalesTax.Models;

namespace NCSalesTaxUnitTests
{
    /// <summary>
    /// Tests for the TaxRatesController class
    /// </summary>
    [TestClass]
    public class TaxRatesControllerTests
    {
        [TestMethod]
        public void TaxRatesController_GetTaxByCounty_NullCounty_BadRequest()
        {
            //setup
            Controllers.TaxRatesController controller = new Controllers.TaxRatesController();

            //act
            ActionResult<Models.TaxCalculatorResponse> response = controller.GetTaxByCounty(string.Empty);

            //assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response.Result, typeof(BadRequestResult));
        }

        [TestMethod]
        public void TaxRatesController_GetTaxByCounty_BadCounty_NotFound()
        {
            //setup
            Controllers.TaxRatesController controller = new Controllers.TaxRatesController();

            //act
            ActionResult<Models.TaxCalculatorResponse> response = controller.GetTaxByCounty("FakeCounty");

            //assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response.Result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void TaxRatesController_GetTaxByCounty_Success()
        {
            //setup
            Controllers.TaxRatesController controller = new Controllers.TaxRatesController();

            //act
            ActionResult<Models.TaxCalculatorResponse> response = controller.GetTaxByCounty("Wake");

            //assert
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response.Result, typeof(OkObjectResult));
            
            Models.TaxCalculatorResponse responseModel = (response.Result as OkObjectResult).Value as Models.TaxCalculatorResponse;
            Assert.AreEqual("4.75%", responseModel.StateTaxRate);
            Assert.AreEqual("2.50%", responseModel.CountyTaxRate);
            Assert.AreEqual("7.25%", responseModel.TotalTaxRate);
        }
    }
}
